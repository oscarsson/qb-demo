Small example programs using qb.

qb is a small library to draw voxels in an SDL window using OpenGL.
A voxel is a "volume pixel", a cube, and these cubes are drawn inside the
space of a larger cube. Like pixels are small squares drawn inside
the boundaries of a larger rectangle (the screen or window).
qb can be found in the qb repository:
https://gitlab.com/oscarsson/qb


The code in this repository is quite unpolished and things can most likely
be done in better ways here and there. It is only meant as examples.

qb-demo is created by Mårten Oscarsson and released under the
GPLv3 license. See LICENSE.