#include <stdio.h>
#include <pthread.h>
#include <SDL2/SDL.h>
#include <sys/queue.h>
#include <unistd.h>
#include <qb.h>

#include "qbdemo_sdl.h"
#include "qbdemo_shapes.h"
#include "qbdemo_burn.h"
#include "qbdemo_camera.h"

/* Macros */
#define PI 3.14159265

/* Typedefs */
typedef struct thread_data {
  volatile int done;
  int w;
  int h;
  int fullscreen;
  int vsync;
  const char *ttf_path;
  int time_it;
} thread_data_t;

/* Local variables */
static pthread_t fire_thread;
static qbd_cam_slider_t *slider_p;
static qbd_brn_fire_t *fire_p;
static qb_icoord_t w_min, w_max, ws;

static qb_icoord_t tbox_size;
static qb_icoord_t tbox_off;
static qb_icoord_t tbox_toff;
static int tbox_tsz;


/* Prototypes */



/* Local functions */
static void draw(qb_context_t *qb_ctx_p)
{
  qb_color_t c_w = {.r = 0xff, .g = 0xff, .b = 0xff};
  qb_color_t c_r = {.r = 0xff, .g = 0x00, .b = 0x00};
  qb_color_t c_y = {.r = 0xcf, .g = 0xcf, .b = 0x00};
  qb_color_t c_b = {.r = 0x00, .g = 0x00, .b = 0xff};
  qb_icoord_t off;
  int w;
  int i;
  qb_color_t s_color;
  static int s_dir = 1;
  static int s_zlen = 1;

  qb_voxel_clear_all(qb_ctx_p);

  qbd_shp_tiled_box_insert(qb_ctx_p, tbox_size, tbox_off, tbox_toff, tbox_tsz,
                           c_y, c_w);

  off.x = -(fire_p->size.x / 2);
  off.y = w_min.y + 10;
  off.z = -(fire_p->size.z / 2);
  qbd_brn_fire_insert(qb_ctx_p, fire_p, off);
  
  qb_draw(qb_ctx_p);
}

static void *fire_fn(void *data)
{
  qbd_sdl_window_t *win_p;
  thread_data_t *data_p = (thread_data_t *)data;
  uint32_t sdl_flags = (SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
  uint32_t qb_flags = data_p->vsync ? QB_INIFLAG_VSYNC : 0;
  qb_context_t *qb_ctx_p;
  qb_color_t bg_color = {.r = 0x45, .g = 0x45, .b = 0x45};
  qb_icoord_t view_size = {.x = 128, .y = 64, .z = 128};
  qb_camera_t camera;
  int use_gap = 0;
  float cam_speed = 1.1f;
  int cam_done = 1;
  int32_t t, last_t, delta_t;
  int64_t time = 0;
  int i = 0;
  int scroll_done = 0;

  if (data_p->fullscreen) {
    sdl_flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
  }
  qbd_sdl_window_open(&win_p, "fire", data_p->w, data_p->h, sdl_flags, 0);
  if (!win_p) {
    goto end;
  }

  /* initialize qb */
  qb_ctx_create(&qb_ctx_p, win_p->window, &view_size, &bg_color, qb_flags);
  if (!qb_ctx_p) {
    goto end_close_win;
  }
    
  /* setup our stuff */
  qb_get_world_bound(qb_ctx_p, &w_min, &w_max);
  ws.x = w_max.x - w_min.x + 1;
  ws.y = w_max.y - w_min.y + 1;
  ws.z = w_max.z - w_min.z + 1;

  qb_camera_get(qb_ctx_p, &camera);
  qbd_cam_slider_create(&slider_p, &camera);

  tbox_size.x = ws.x;
  tbox_size.y = 10;
  tbox_size.z = ws.z;
  tbox_off = w_min;
  tbox_tsz = ws.x / 18;
  tbox_toff.x = (ws.x % tbox_tsz) / 2;
  tbox_toff.y = 0;
  tbox_toff.z = (ws.z % tbox_tsz) / 2;

  qb_icoord_t f_size = {.x = 60, .y = 30, .z = 60};
  qb_color_t f_col_cool = {.r = 44, .g = 0, .b = 0};
  qb_color_t f_col_mid = {.r = 255, .g = 255, .b = 0};
  qb_color_t f_col_hot = {.r = 255, .g = 255, .b = 255};

  qbd_brn_fire_create(&fire_p, f_size, f_col_cool, f_col_mid, f_col_hot, 25, 12,
                      80, 35.0);

  /* main loop.. check events and draw */
  while (1) {
    /* time each lap */
    t = (int32_t)SDL_GetTicks();
    delta_t = t - last_t;
    last_t = t;

    /* ..and maybe measure fps too */
    if (data_p->time_it) {
      time += delta_t;
      i++;
      if (i > 0 && (i % 100 == 0)) {
        printf("%ffps\n", 100000/((float)time));
        time = 0;
      }
    }

    /* handle events and act accordingly */
    qbd_sdl_handle_events(win_p);
    if (win_p->key_pressed[SDL_SCANCODE_ESCAPE] ||
        win_p->quit) {
      break;
    }

    if (win_p->key_events[SDL_SCANCODE_RIGHT] & QBD_SDL_KEYEVENT_DOWN) {
      qbd_cam_slider_flip_side(slider_p, QBD_CAM_DIRECTION_RIGHT, cam_speed);
      cam_done = 0;
    }
    if (win_p->key_events[SDL_SCANCODE_LEFT] & QBD_SDL_KEYEVENT_DOWN) {
      qbd_cam_slider_flip_side(slider_p, QBD_CAM_DIRECTION_LEFT, cam_speed);
      cam_done = 0;
    }

    if (win_p->key_events[SDL_SCANCODE_UP] & QBD_SDL_KEYEVENT_DOWN) {
      cam_speed = cam_speed < 2.0f ? cam_speed * 1.1f : 2.0f;
    }
    if (win_p->key_events[SDL_SCANCODE_DOWN] & QBD_SDL_KEYEVENT_DOWN) {
      cam_speed = cam_speed > 0.1f ? cam_speed / 1.1f : 0.1f;
    }

    if (win_p->key_events[SDL_SCANCODE_G] & QBD_SDL_KEYEVENT_DOWN) {
      use_gap = use_gap ? 0 : 1;
      qbd_cam_set_gap(qb_ctx_p, use_gap);
    }

    if (win_p->key_events[SDL_SCANCODE_F] & QBD_SDL_KEYEVENT_DOWN) {
      data_p->fullscreen = data_p->fullscreen ? 0 : 1;
      qbd_sdl_window_set_fullscreen(win_p, data_p->fullscreen);
    }
 
    if (win_p->mouse.buttons_pressed & QBD_SDL_MOUSE_BL_BIT) {
      qbd_sdl_mouse_set_relative_mode(win_p, 1);
      if (win_p->mouse.x_rel) {
        float a = (((float)win_p->mouse.x_rel * PI) / 250.0f);
        qbd_cam_slider_add_angle(slider_p, a, cam_speed);
        cam_done = 0;
      }
    } else {
      qbd_sdl_mouse_set_relative_mode(win_p, 0);
    }

    /* we want fresh data next lap, clear current values */
    qbd_sdl_key_clear_events(win_p);
    qbd_sdl_mouse_clear_relative_xy(win_p);

    /* update moving stuff according to time delta */
    qbd_brn_fire_advance(fire_p, (float)delta_t / 1000.0f);

    if (!cam_done) {
      cam_done = qbd_cam_slider_advance(slider_p, (float)delta_t / 1000.0f);
      qbd_cam_slider_apply(qb_ctx_p, slider_p);
    }

    /* draw the voxels */
    draw(qb_ctx_p);
  }

  qbd_brn_fire_destroy(&fire_p);
  qbd_cam_slider_destroy(&slider_p);
end_destroy_qb:
  qb_ctx_destroy(&qb_ctx_p);
end_close_win:
  qbd_sdl_window_close(&win_p);

end:
  pthread_detach(pthread_self());
  data_p->done = 1;

  return (void *)0;
}

static void help()
{
  printf("Optins\n");
  printf("-h          print this\n");
  printf("-f          fullscreen\n");
  printf("-r a|b|c|d  a:1920x1080 b:1280x720 c:1024x768 d:640x480\n");
  printf("-t          print timing info (in shell)\n");
  printf("-v          use vsync\n");
}

/* Interface */
int main(int argc, char *argv[])
{
  int quit = 0;
  thread_data_t fire = {0};
  int ret, opt;
  char c;

  while ((opt = getopt(argc, argv, "hfr:tv")) >= 0) {
    switch (opt) {
      case 'h':
        help();
        return 0;
      case 'f':
        fire.fullscreen = 1;
        break;
      case 'r':
        c = argv[optind-1][0];
        if (c == 'a') {
          fire.w = 1920;
          fire.h = 1080;
        } else if (c == 'b') {
          fire.w = 1280;
          fire.h = 720;
        } else if (c == 'c') {
          fire.w = 1024;
          fire.h = 768;
        } else if (c == 'd') {
          fire.w = 640;
          fire.h = 480;
        } else {
          help();
          return 0;
        }
        break;
      case 't':
        fire.time_it = 1;
        break;
      case 'v':
        fire.vsync = 1;
        break;
      default:
        help();
        return 0;
    }
  }

  if (qbd_sdl_init(&quit)) {
    return 1;
  }

  ret = pthread_create(&fire_thread, NULL, fire_fn, (void *)&fire);
  if(ret) {
    printf("Could not create fire thread\n");
    goto end;
  }

  /* pump the events and stay in event loop untill
     quit event received or thread ends */
  while(!quit && !fire.done) {
    qbd_sdl_pump_events();
    usleep(10000);
  }
end:
  qbd_sdl_exit();

  return 0;
}

/* tmporary stuff */
