#ifndef __QBDEMO_BURN_H_
#define __QBDEMO_BURN_H_

#include <qb.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Macros */

/* Typedefs */
typedef struct qbd_brn_fire {
  qb_icoord_t size;
  qb_color_t colors[100];
  int *flames;
  int num_heat;
  qb_icoord_t *heat;
  int intensity;
  int cooling;
  float delta;
  float speed;
} qbd_brn_fire_t;


/* Interface */
int qbd_brn_fire_create(qbd_brn_fire_t **fire_pp, qb_icoord_t size,
                        qb_color_t col_cool, qb_color_t col_mid,
                        qb_color_t col_hot, int heat, int cooling,
                        int intensity, float speed);
void qbd_brn_fire_destroy(qbd_brn_fire_t **fire_pp);
int qbd_brn_fire_advance(qbd_brn_fire_t *fire_p, float time);
void qbd_brn_fire_insert(qb_context_t *qb_ctx_p,
                         qbd_brn_fire_t *fire_p, qb_icoord_t off);

#ifdef __cplusplus
}
#endif

#endif // __QBDEMO_BURN_H_
