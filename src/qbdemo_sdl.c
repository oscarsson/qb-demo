#include "qbdemo_sdl.h"

/* Macros */
#define MOUSE_CLICK_TIMEOUT     200


/* Typedefs */
/* define the 'struct window_list_head'. stores all opened windows */
TAILQ_HEAD(window_list_head, qbd_sdl_window);
typedef struct window_list_head window_list_head_t;

/* stored on the 'event_list' in each window */
typedef struct event
{
  TAILQ_ENTRY(event) event_list;
  SDL_Event event;
} event_t;


/* Local variables */
static window_list_head_t window_head;
static pthread_mutex_t window_mutex;
static int *quit_p;

/* Prototypes */


/* Local functions */
static int event_cb(void* data, SDL_Event* event)
{
  event_t *ev_p;
  qbd_sdl_window_t *win_p;
  uint32_t window_id;

  switch(event->type) {
    case SDL_WINDOWEVENT:
      window_id = event->window.windowID;
      break;
    case SDL_KEYUP:
    case SDL_KEYDOWN:
      window_id = event->key.windowID;
      break;
    case SDL_MOUSEMOTION:
      window_id = event->motion.windowID;
      break;
      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP:
        window_id = event->button.windowID;
        break;
      case SDL_MOUSEWHEEL:
        window_id = event->wheel.windowID;
        break;
    case SDL_QUIT:
      if (quit_p) {
        *quit_p = 1;
      }
    default:
      goto out;
  }

  pthread_mutex_lock(&window_mutex);
  TAILQ_FOREACH(win_p, &window_head, window_list) {
    if (window_id != win_p->window_id) {
      continue;
    }

    if (win_p->skip_events) {
      /* always listen to window close events */
      if (event->type == SDL_WINDOWEVENT &&
          event->window.event == SDL_WINDOWEVENT_CLOSE) {
        win_p->quit = 1;
      }
      break;
    }

    ev_p = malloc(sizeof(event_t));
    ev_p->event = *event;

    pthread_mutex_lock(&win_p->event_mutex);
    TAILQ_INSERT_TAIL(&win_p->event_head, ev_p, event_list);
    pthread_mutex_unlock(&win_p->event_mutex);
    break;
  }
  pthread_mutex_unlock(&window_mutex);
out:

  return 0; /* drop */
}

static void register_for_events(void)
{
  SDL_AddEventWatch(event_cb, NULL);
}

static void unregister_for_events(void)
{
  SDL_DelEventWatch(event_cb, NULL);
}

static void handle_window_event(qbd_sdl_window_t *win_p,
                                const SDL_WindowEvent *window)
{
  switch (window->event) {
    case SDL_WINDOWEVENT_FOCUS_GAINED:
      win_p->window_active = 1;
      break;
    case SDL_WINDOWEVENT_FOCUS_LOST:
      win_p->window_active = 0;
      break;
    case SDL_WINDOWEVENT_CLOSE:
      win_p->quit = 1;
      break;
    default:
      break;
  }
}

static inline int sdl_to_qbd_buttons_bits(int sdl_buttons)
{
  int btn = 0;

  if (sdl_buttons & SDL_BUTTON(SDL_BUTTON_LEFT)) {
    btn |= QBD_SDL_MOUSE_BL_BIT;
  }
  if (sdl_buttons & SDL_BUTTON(SDL_BUTTON_MIDDLE)) {
    btn |= QBD_SDL_MOUSE_BM_BIT;
  }
  if (sdl_buttons & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
    btn |= QBD_SDL_MOUSE_BR_BIT;
  }
  return btn;
}

static void handle_mouse_motion_event(qbd_sdl_window_t *win_p,
                                      const SDL_MouseMotionEvent *motion)
{
  int i;
  int event_time = (int)motion->timestamp;
  int qbd_btn_bits[3] = {QBD_SDL_MOUSE_BL_BIT,
                         QBD_SDL_MOUSE_BM_BIT,
                         QBD_SDL_MOUSE_BR_BIT};
  int sdl_btn_bits[3] = {SDL_BUTTON(SDL_BUTTON_LEFT),
                         SDL_BUTTON(SDL_BUTTON_MIDDLE),
                         SDL_BUTTON(SDL_BUTTON_RIGHT)};

  win_p->mouse.buttons_pressed = sdl_to_qbd_buttons_bits(motion->state);
  win_p->mouse.x = motion->x;
  win_p->mouse.y = motion->y;
  win_p->mouse.x_rel = motion->xrel;
  win_p->mouse.y_rel = motion->yrel;

  for (i = 0; i < 3; i++) {
    if ((motion->state & sdl_btn_bits[i]) &&
        (event_time - win_p->mouse.click_start_time[i] > MOUSE_CLICK_TIMEOUT)) {
      win_p->mouse.buttons_drag |= qbd_btn_bits[i];
    } else {
      win_p->mouse.buttons_drag &= ~qbd_btn_bits[i];
    }
  }
}

static inline int sdl_to_qbd_button(int sdl_button)
{
  int btn = 0;

  if (sdl_button == SDL_BUTTON_LEFT) {
    return QBD_SDL_MOUSE_BL;
  }
  if (sdl_button == SDL_BUTTON_MIDDLE) {
    return QBD_SDL_MOUSE_BM;
  }
  if (sdl_button == SDL_BUTTON_RIGHT) {
    return QBD_SDL_MOUSE_BR;
  }

  return -1;
}

static void handle_mouse_button_event(qbd_sdl_window_t *win_p,
                                      const SDL_MouseButtonEvent *button)
{
  int qbd_btn = sdl_to_qbd_button(button->button);
  int event_time = (int)button->timestamp;
  int qbd_btn_bits[3] = {QBD_SDL_MOUSE_BL_BIT,
                         QBD_SDL_MOUSE_BM_BIT,
                         QBD_SDL_MOUSE_BR_BIT};

  if (qbd_btn == -1) {
    /* not in our buttons */
    return;
  }

  if (button->state == SDL_PRESSED) {
    win_p->mouse.click_start_time[qbd_btn] = event_time;
  } else {
    win_p->mouse.buttons_drag &= ~qbd_btn_bits[qbd_btn];
    if (event_time - win_p->mouse.click_start_time[qbd_btn] >
        MOUSE_CLICK_TIMEOUT) {
      win_p->mouse.clicks[qbd_btn] = 0;
    } else {
      win_p->mouse.clicks[qbd_btn] = button->clicks;
    }
  }
}

static void handle_mouse_wheel_event(qbd_sdl_window_t *win_p,
                                     const SDL_MouseWheelEvent *wheel)
{
  win_p->mouse.x_wheel = wheel->x;
  win_p->mouse.y_wheel = wheel->y;
}

static void handle_key_event(qbd_sdl_window_t *win_p,
                             const SDL_KeyboardEvent *key)
{
  if (key->state == SDL_PRESSED) {
    /* for keys with auto repeat we get several down events
       with no up event betwean */
    if (!win_p->key_pressed[key->keysym.scancode]) {
      win_p->key_events[key->keysym.scancode] |= QBD_SDL_KEYEVENT_DOWN;
    } else {
      win_p->key_events[key->keysym.scancode] |= QBD_SDL_KEYEVENT_DOWN_REPEAT;
    }
  } else {
    win_p->key_events[key->keysym.scancode] |= QBD_SDL_KEYEVENT_UP;
  }
  win_p->key_pressed[key->keysym.scancode] = (key->state == SDL_PRESSED);
}


/* Interface */
int qbd_sdl_init(int *got_quit_event_p)
{
  TAILQ_INIT(&window_head);
  pthread_mutex_init(&window_mutex, NULL);

  /* initialize sdl */
  if (SDL_Init(SDL_INIT_VIDEO)) {
        printf("Error initializing SDL:  %s\n", SDL_GetError());
        return 1;
  }
  quit_p = got_quit_event_p;
  register_for_events();

  return 0;
}

void qbd_sdl_exit(void)
{
  unregister_for_events();
  // CLOSE ALL WINDOWS??

  pthread_mutex_destroy(&window_mutex);
  SDL_Quit();
}

void qbd_sdl_window_open(qbd_sdl_window_t **win_pp,
                            const char* title, int w, int h, uint32_t flags,
                            int skip_events)
{
  qbd_sdl_window_t *win_p;
  SDL_DisplayMode mode;

  if ((w < 1) || (h < 1) ||
    (flags & SDL_WINDOW_FULLSCREEN_DESKTOP) ==
      SDL_WINDOW_FULLSCREEN_DESKTOP) {
    SDL_GetDesktopDisplayMode(0, &mode);
    w = mode.w;
    h = mode.h;
  }

  win_p = calloc(sizeof(qbd_sdl_window_t), 1);
  win_p->window = SDL_CreateWindow(title,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   w, h, flags);

  if (win_p->window == NULL) {
    printf("Error creating window: %s\n", SDL_GetError());
    *win_pp = NULL;
    return;
  }
  win_p->window_id = SDL_GetWindowID(win_p->window);
  pthread_mutex_init(&win_p->event_mutex, NULL);
  TAILQ_INIT(&win_p->event_head);
  win_p->skip_events = skip_events;

  pthread_mutex_lock(&window_mutex);
  TAILQ_INSERT_TAIL(&window_head, win_p, window_list);
  pthread_mutex_unlock(&window_mutex);

  *win_pp = win_p;
}

void qbd_sdl_window_close(qbd_sdl_window_t **win_pp)
{
  qbd_sdl_window_t *win_p = *win_pp;

  pthread_mutex_lock(&window_mutex);
  TAILQ_REMOVE(&window_head, win_p, window_list);
  pthread_mutex_unlock(&window_mutex);

  SDL_SetRelativeMouseMode(SDL_FALSE);
  SDL_DestroyWindow(win_p->window);

  pthread_mutex_destroy(&win_p->event_mutex);
  free(win_p);
  *win_pp = NULL;
}

void qbd_sdl_window_set_fullscreen(qbd_sdl_window_t *win_p, int fullscreen)
{
  if (fullscreen) {
    SDL_SetWindowFullscreen(win_p->window, SDL_WINDOW_FULLSCREEN);
  } else {
    SDL_SetWindowFullscreen(win_p->window, 0);
  }
}

void qbd_sdl_pump_events(void)
{
  SDL_PumpEvents();
}

void qbd_sdl_handle_events(qbd_sdl_window_t *win_p)
{
  event_t *ev_p;

  pthread_mutex_lock(&win_p->event_mutex);
  while (!TAILQ_EMPTY(&win_p->event_head)) {
    ev_p = TAILQ_FIRST(&win_p->event_head);
    switch(ev_p->event.type) {
      case SDL_WINDOWEVENT:
        handle_window_event(win_p, &ev_p->event.window);
        break;
      case SDL_KEYUP:
      case SDL_KEYDOWN:
        handle_key_event(win_p, &ev_p->event.key);
        break;
      case SDL_MOUSEMOTION:
        handle_mouse_motion_event(win_p, &ev_p->event.motion);
        break;
      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP:
        handle_mouse_button_event(win_p, &ev_p->event.button);
        break;
      case SDL_MOUSEWHEEL:
        handle_mouse_wheel_event(win_p, &ev_p->event.wheel);
        break;
      default:
        break;
    }
    TAILQ_REMOVE(&win_p->event_head, ev_p, event_list);
    free(ev_p);
  }
  pthread_mutex_unlock(&win_p->event_mutex);
}

void qbd_sdl_key_clear_events(qbd_sdl_window_t *win_p)
{
  int i;

  for (i = 0; i < SDL_NUM_SCANCODES; i++) {
    win_p->key_events[i] = 0;
  }
}

void qbd_sdl_mouse_set_relative_mode(qbd_sdl_window_t *win_p, int be_relative)
{
  if (win_p->mouse.relative_mode == be_relative) {
    return;
  }

  win_p->mouse.relative_mode = be_relative;
  if (be_relative) {
    SDL_SetRelativeMouseMode(SDL_TRUE);
    win_p->mouse.x_saved = win_p->mouse.x;
    win_p->mouse.y_saved = win_p->mouse.y;
  } else {
    SDL_SetRelativeMouseMode(SDL_FALSE);
    SDL_WarpMouseInWindow(NULL, win_p->mouse.x_saved, win_p->mouse.y_saved);
  }
}

void qbd_sdl_mouse_clear_relative_xy(qbd_sdl_window_t *win_p)
{
  win_p->mouse.x_rel = 0;
  win_p->mouse.y_rel = 0;
}


/* tmporary stuff */

