#include <unistd.h>

#include "qbdemo_burn.h"

/* Macros */

/* Typedefs */

/* Local variables */

/* Prototypes */

/* Local functions */
static void fill_color_range(qb_color_t *range, int num,
                             qb_color_t start_color, qb_color_t end_color)
{
  qb_color_t color;
  float r_delta, g_delta, b_delta;
  float r, g, b;
  int i;

  range[0] = start_color;
  if (num == 1) {
    return;
  }
  range[num-1] = end_color;
  if (num == 2) {
    return;
  }

  /* how many is there from the first to and including the last */
  num--;

  r_delta = ((float)end_color.r - (float)start_color.r)/(float)num;
  g_delta = ((float)end_color.g - (float)start_color.g)/(float)num;
  b_delta = ((float)end_color.b - (float)start_color.b)/(float)num;

  /* the last one have already got its color */
  num--;

  while (num >= 1) {
    r = (float)start_color.r + (r_delta * (float)num);
    g = (float)start_color.g + (g_delta * (float)num);
    b = (float)start_color.b + (b_delta * (float)num);
    if (r < 0.0) r = 0.0;
    if (r > 255.0) r = 255.0;
    if (g < 0.0) g = 0.0;
    if (g > 255.0) g = 255.0;
    if (b < 0.0) b = 0.0;
    if (b > 255.0) b = 255.0;

    color.r = (unsigned char)r;
    color.g = (unsigned char)g;
    color.b = (unsigned char)b;

    range[num] = color;
    num--;
  }
}

static inline void set_temp_on(qbd_brn_fire_t *fire_p, int x, int y, int z,
                              int t)
{
  fire_p->flames[z * fire_p->size.y * fire_p->size.x +
                 y * fire_p->size.x + x] = t;
}

static inline int get_temp_on(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  return fire_p->flames[z * fire_p->size.y * fire_p->size.x +
                        y * fire_p->size.x + x];
}

static inline int get_temp_xn(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  if (x <= 0) {
    return 0;
  }
  return fire_p->flames[z * fire_p->size.y * fire_p->size.x +
                        y * fire_p->size.x + x - 1];
}

static inline int get_temp_xp(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  if (x >= fire_p->size.x - 1) {
    return 0;
  }
  return fire_p->flames[z * fire_p->size.y * fire_p->size.x +
                        y * fire_p->size.x + x + 1];
}

static inline int get_temp_yn(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  if (y <= 0) {
    return 0;
  }
  return fire_p->flames[z * fire_p->size.y * fire_p->size.x +
                        (y - 1) * fire_p->size.x + x];
}

static inline int get_temp_yp(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  if (y >= fire_p->size.y - 1) {
    return 0;
  }
  return fire_p->flames[z * fire_p->size.y * fire_p->size.x +
                        (y + 1) * fire_p->size.x + x];
}

static inline int get_temp_zn(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  if (z <= 0) {
    return 0;
  }
  return fire_p->flames[(z - 1) * fire_p->size.y * fire_p->size.x +
                        y * fire_p->size.x + x];
}

static inline int get_temp_zp(qbd_brn_fire_t *fire_p, int x, int y, int z)
{
  if (z >= fire_p->size.z - 1) {
    return 0;
  }
  return fire_p->flames[(z + 1) * fire_p->size.y * fire_p->size.x +
                        y * fire_p->size.x + x];
}

static void set_rand_heat_pos(qbd_brn_fire_t *fire_p, int i)
{
  fire_p->heat[i].y = 0;
  fire_p->heat[i].x = rand() % fire_p->size.x;
  fire_p->heat[i].z = rand() % fire_p->size.z;
}

static void move_rand_heat_pos(qbd_brn_fire_t *fire_p, int i)
{
  int moved = 0, tried = 0;
  qb_icoord_t pos;
  int d;

  while (!moved && tried != 0xf) {
    pos = fire_p->heat[i];
    d = rand() % 4;
    switch (d) {
      case 0:
        if (pos.x <= 0 || (tried & 1)) {
          tried |= 1;
          continue;
        }
        pos.x -= 1;
        moved = 1;
        break;
      case 1:
        if (pos.x >= fire_p->size.x - 1 || (tried & 2)) {
          tried |= 2;
          continue;
        }
        pos.x += 1;
        moved = 1;
        break;
      case 2:
        if (pos.z <= 0 || (tried & 4)) {
          tried |= 4;
          continue;
        }
        pos.z -= 1;
        moved = 1;
        break;
      case 3:
        if (pos.z >= fire_p->size.z - 1 || (tried & 8)) {
          tried |= 8;
          continue;
        }
        pos.z += 1;
        moved = 1;
        break;
      default:
        continue;
    }
  }

  fire_p->heat[i] = pos;
}

static void insert_heat(qbd_brn_fire_t *fire_p, int i)
{
  qb_icoord_t pos = fire_p->heat[i];

  set_temp_on(fire_p, pos.x, 0, pos.z, 99);
}

/* Interface */
int qbd_brn_fire_create(qbd_brn_fire_t **fire_pp, qb_icoord_t size,
                        qb_color_t col_cool, qb_color_t col_mid,
                        qb_color_t col_hot, int heat, int cooling,
                        int intensity, float speed)
{
  qbd_brn_fire_t *fire_p;
  int i;

  heat = heat < 100 ? heat : 100;
  intensity = intensity < 100 ? intensity : 100;

  fire_p = malloc(sizeof(qbd_brn_fire_t));
  fire_p->size = size;
  fill_color_range(&fire_p->colors[0], 50, col_cool, col_mid);
  fill_color_range(&fire_p->colors[49], 51, col_mid, col_hot);
  fire_p->flames = calloc(size.x * size.y * size.z, sizeof(int));
  fire_p->intensity = intensity;
  fire_p->cooling = cooling;
  fire_p->delta = 0.0f;
  fire_p->speed = speed;

  fire_p->num_heat = (heat * size.x * size.z) / 100;
  fire_p->heat = calloc(fire_p->num_heat, sizeof(qb_icoord_t));

  for (i = 0; i < fire_p->num_heat; i++) {
    set_rand_heat_pos(fire_p, i);
  }

  *fire_pp = fire_p;
  return 0;
}

void qbd_brn_fire_destroy(qbd_brn_fire_t **fire_pp)
{
  qbd_brn_fire_t *fire_p = *fire_pp;

  free(fire_p->flames);
  free(fire_p->heat);
  free(fire_p);
  *fire_pp = NULL;
}

int qbd_brn_fire_advance(qbd_brn_fire_t *fire_p, float time)
{
  qb_voxel_t vox;
  int x, y, z;
  int temp;
  int flame;
  int i = 0;

  fire_p->delta += time * fire_p->speed;
  while (fire_p->delta >= 1.0f) {
    fire_p->delta -= 1.0f;
    for (y = fire_p->size.y - 1; y >= 1; y--) {
      for (z = 0; z < fire_p->size.z; z++) {
        for (x = 0; x < fire_p->size.x; x++) {
          temp =  get_temp_xn(fire_p, x, y - 1, z);
          temp += get_temp_xp(fire_p, x, y - 1, z);
          temp += 6 * get_temp_yn(fire_p, x, y, z);
          temp += get_temp_zn(fire_p, x, y - 1, z);
          temp += get_temp_zp(fire_p, x, y - 1, z);
          temp -= fire_p->cooling;
          temp = (temp / 10);
          temp = temp > 0 ? temp : 0;
          set_temp_on(fire_p, x, y, z, temp);
        }
      }
    }

    for (z = 0; z < fire_p->size.z; z++) {
      for (x = 0; x < fire_p->size.x; x++) {
        temp = get_temp_on(fire_p, x, 0, z);
        set_temp_on(fire_p, x, 0, z, temp / 2);
      }
    }

    for (i = 0; i < fire_p->num_heat; i++) {
      if ((rand() % 101) < fire_p->intensity) {
        move_rand_heat_pos(fire_p, i);
      }
      insert_heat(fire_p, i);
    }
  }

  return 0;
}

void qbd_brn_fire_insert(qb_context_t *qb_ctx_p,
                         qbd_brn_fire_t *fire_p, qb_icoord_t off)
{
  qb_voxel_t vox;
  int temp;

  /* don't insert lowest row, shift everything down (y + 1) */
  for (vox.pos.y = 0; vox.pos.y < (fire_p->size.y - 1); vox.pos.y++) {
    for (vox.pos.z = 0; vox.pos.z < fire_p->size.z; vox.pos.z++) {
      for (vox.pos.x = 0; vox.pos.x < fire_p->size.x; vox.pos.x++) {
        temp = get_temp_on(fire_p, vox.pos.x, vox.pos.y + 1, vox.pos.z);
        if (!temp) {
          continue;
        }
        vox.color = fire_p->colors[temp];
        qb_voxel_set(qb_ctx_p, &vox, &off);
      }
    }
  }
}


/* tmporary stuff */

