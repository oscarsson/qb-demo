#include <unistd.h>
#include <qb.h>

/* Macros */

/* Typedefs */

/* Local variables */

/* Prototypes */

/* Local functions */
static void draw_box(qb_context_t *qb_ctx_p,
                     qb_icoord_t size, qb_icoord_t off, qb_color_t color)
{
  qb_voxel_t vox;

  vox.color = color;
  for (vox.pos.y = 0; vox.pos.y < size.y; vox.pos.y++) {
    for (vox.pos.z = 0; vox.pos.z < size.z; vox.pos.z++) {
      for (vox.pos.x = 0; vox.pos.x < size.x; vox.pos.x++) {
        qb_voxel_set(qb_ctx_p, &vox, &off);
      }
    }
  }
}

static inline void __tb_set_start_values(int16_t *color_index_p,
                                         int16_t *first_size_p,
                                         int16_t tile_off, int tile_size)
{
  tile_off = tile_off % (tile_size * 2);

  if (tile_off < 0) {
    tile_off += (tile_size * 2);
  }

  if (tile_off == 0) {
    *first_size_p = tile_size;
    return;
  }

  if (tile_off <= tile_size) {
    *first_size_p = tile_off;
    (*color_index_p)++;
    return;
  }

  *first_size_p = tile_off - tile_size;
}

/* Interface */
void qbd_shp_box_insert(qb_context_t *qb_ctx_p,
                        qb_icoord_t size, qb_icoord_t off, qb_color_t color)
{
  draw_box(qb_ctx_p, size, off, color);
}

void qbd_shp_tiled_box_insert(qb_context_t *qb_ctx_p,
                              qb_icoord_t size, qb_icoord_t offset,
                              qb_icoord_t tile_offset, int tile_size,
                              qb_color_t color1, qb_color_t color2)
{
  int c_index;
  qb_icoord_t c;
  qb_icoord_t start_c = {0};
  qb_icoord_t draw_sz;
  qb_icoord_t first_sz;
  qb_icoord_t end;
  qb_color_t colors[2];
  qb_icoord_t off;

  colors[0] = color1;
  colors[1] = color2;

  __tb_set_start_values(&start_c.x, &first_sz.x, tile_offset.x, tile_size);
  __tb_set_start_values(&start_c.y, &first_sz.y, tile_offset.y, tile_size);
  __tb_set_start_values(&start_c.z, &first_sz.z, tile_offset.z, tile_size);

  end.x = offset.x + size.x;
  end.y = offset.y + size.y;
  end.z = offset.z + size.z;

  draw_sz.z = first_sz.z;
  off.z = offset.z;
  for (c.z = start_c.z; ;c.z++) {
    if (off.z + draw_sz.z >= end.z) {
      draw_sz.z = end.z - off.z;
    }
    if (draw_sz.z < 1) {
      break;
    }

    draw_sz.y = first_sz.y;
    off.y = offset.y;
    for (c.y = start_c.y; ; c.y++) {
      if (off.y + draw_sz.y >= end.y) {
        draw_sz.y = end.y - off.y;
      }
      if (draw_sz.y < 1) {
        break;
      }

      draw_sz.x = first_sz.x;
      off.x = offset.x;
      for (c.x = start_c.x; ; c.x++) {
        if (off.x + draw_sz.x >= end.x) {
          draw_sz.x = end.x - off.x;
        }
        if (draw_sz.x < 1) {
          break;
        }

        c_index = (c.x + c.y + c.z) % 2;
        draw_box(qb_ctx_p, draw_sz, off, colors[c_index]);

        off.x += draw_sz.x;
        draw_sz.x = tile_size;
      }
      off.y += draw_sz.y;
      draw_sz.y = tile_size;
    }
    off.z += draw_sz.z;
    draw_sz.z = tile_size;
  }
}

/* tmporary stuff */




