#include <unistd.h>

#include "qbdemo_camera.h"

/* Macros */
#define PI 3.14159265
#define ANGLE_MARGIN   (0.0001f)
#define ANGLE_ZP       (0.0f)
#define ANGLE_ZP360    (2.0f * PI)
#define ANGLE_XP       (PI/2.0f)
#define ANGLE_ZN       (PI)
#define ANGLE_XN       (3.0f * PI / 2.0f)
#define ANGLE_ZPL      (ANGLE_ZP360 - ANGLE_MARGIN)
#define ANGLE_ZPR      (ANGLE_ZP + ANGLE_MARGIN)
#define ANGLE_XPL      (ANGLE_XP - ANGLE_MARGIN)
#define ANGLE_XPR      (ANGLE_XP + ANGLE_MARGIN)
#define ANGLE_ZNL      (ANGLE_ZN - ANGLE_MARGIN)
#define ANGLE_ZNR      (ANGLE_ZN + ANGLE_MARGIN)
#define ANGLE_XNL      (ANGLE_XN - ANGLE_MARGIN)
#define ANGLE_XNR      (ANGLE_XN + ANGLE_MARGIN)

/* Typedefs */

/* Local variables */

/* Prototypes */

/* Local functions */

/* Interface */
int qbd_cam_slider_create(qbd_cam_slider_t **slider_pp, qb_camera_t *cam_p)
{
  float x, y, z;
  qbd_cam_slider_t *slider_p = malloc(sizeof(qbd_cam_slider_t));

  if (!cam_p) {
    printf("No start camera\n");
    return 1;
  }
  x = cam_p->pos.x - cam_p->target.x;
  y = cam_p->pos.y - cam_p->target.y;
  z = cam_p->pos.z - cam_p->target.z;

  slider_p->y = y;
  slider_p->xz_dist = sqrt(x * x + z * z);
  if (z > 0.0f) {
    if (x > 0.0f) {
      slider_p->angle = asin(x / slider_p->xz_dist);
    } else {
      slider_p->angle = 2.0f * PI + asin(x / slider_p->xz_dist);
    }
  } else {
      slider_p->angle = PI - asin(x / slider_p->xz_dist);
  }
  if (slider_p->angle >= 2.0f * PI) {
    slider_p->angle -= 2.0f * PI;
  }
  slider_p->wanted_angle = slider_p->angle;
  slider_p->time_left = 0.0f;

  *slider_pp = slider_p;
  return 0;
}

void qbd_cam_slider_destroy(qbd_cam_slider_t **slider_pp)
{
  qbd_cam_slider_t *slider_p = *slider_pp;

  free(slider_p);
  *slider_pp = NULL;
}

void qbd_cam_slider_to_side(qbd_cam_slider_t *slider_p, int side, float speed)
{
  float delta_a;
  switch (side) {
    case QB_SIDE_ZN:
      slider_p->wanted_angle = PI;
      break;
    case QB_SIDE_ZP:
      slider_p->wanted_angle = 0.0f;
      break;
    case QB_SIDE_XN:
      slider_p->wanted_angle = 2.0f * PI - (PI / 2.0f);
      break;
    case QB_SIDE_XP:
      slider_p->wanted_angle = PI / 2.0f;
      break;
    default:
      return;
  }

  delta_a = fabs(slider_p->wanted_angle - slider_p->angle);
  if (delta_a > PI) {
    delta_a = (2.0f * PI) - delta_a;
  }
  if (speed > 0.000001f) {
    slider_p->time_left = delta_a / (2.0f * PI * speed);
  } else {
    slider_p->time_left = 0.0f;
  }
}

void qbd_cam_slider_flip_side(qbd_cam_slider_t *slider_p, int dir, float speed)
{
  int side;
  float a = slider_p->angle;

  if (dir == QBD_CAM_DIRECTION_RIGHT) {
    if (a >= ANGLE_ZPL || (a >= ANGLE_ZP && a < ANGLE_XPL)) {
      side = QB_SIDE_XP;
      goto out;
    }
    if (a >= ANGLE_XPL && a < ANGLE_ZNL) {
      side = QB_SIDE_ZN;
      goto out;
    }
    if (a >= ANGLE_ZNL && a < ANGLE_XNL) {
      side = QB_SIDE_XN;
      goto out;
    }
    if (a >= ANGLE_XNL && a < ANGLE_ZPL) {
      side = QB_SIDE_ZP;
      goto out;
    }
  } else { /* left */
    if (a >= ANGLE_XNR || a < ANGLE_ZPR) {
      side = QB_SIDE_XN;
      goto out;
    }
    if (a >= ANGLE_ZNR && a < ANGLE_XNR) {
      side = QB_SIDE_ZN;
      goto out;
    }
    if (a >= ANGLE_XPR && a < ANGLE_ZNR) {
      side = QB_SIDE_XP;
      goto out;
    }
    if (a < ANGLE_XPR) {
      side = QB_SIDE_ZP;
      goto out;
    }
  }

  return;
out:
  qbd_cam_slider_to_side(slider_p, side, speed);
}

void qbd_cam_slider_add_angle(qbd_cam_slider_t *slider_p,
                              float angle, float speed)
{
  float delta_a;

  slider_p->wanted_angle = slider_p->angle + angle;

  delta_a = fabs(angle);
  if (delta_a > PI) {
    delta_a = (2.0f * PI) - delta_a;
  }
  if (speed > 0.000001f) {
    slider_p->time_left = delta_a / (2.0f * PI * speed);
  } else {
    slider_p->time_left = 0.0f;
  }
}

int qbd_cam_slider_advance(qbd_cam_slider_t *slider_p, float time)
{
  float delta_a;

  if (time >= slider_p->time_left) {
    slider_p->angle = slider_p->wanted_angle;
    slider_p->time_left = 0.0f;
    return 1;
  }

  delta_a = slider_p->wanted_angle - slider_p->angle;
  if (delta_a > PI) {
    delta_a = delta_a - (2.0f * PI);
  }
  if (delta_a < -PI) {
    delta_a = delta_a + (2.0f * PI);
  }

  delta_a *= (time / slider_p->time_left);
  slider_p->angle += delta_a;
  slider_p->time_left -= time;

  return 0;
}

void qbd_cam_slider_apply(qb_context_t *qb_ctx_p, qbd_cam_slider_t *slider_p)
{
  qb_camera_t cam;

  qb_camera_get(qb_ctx_p, &cam);

  cam.pos.y = slider_p->y + cam.target.y;
  cam.pos.x = slider_p->xz_dist * sin(slider_p->angle) + cam.target.x;
  cam.pos.z = slider_p->xz_dist * cos(slider_p->angle) + cam.target.z;

  qb_camera_set(qb_ctx_p, &cam);
}

void qbd_cam_set_gap(qb_context_t *qb_ctx_p, int use_gap)
{
  qb_camera_t camera;

  qb_camera_get(qb_ctx_p, &camera);

  if (use_gap) {
    camera.flags |= QB_CAMFLAG_GAP;
  } else {
    camera.flags &= ~QB_CAMFLAG_GAP;
  }
  qb_camera_set(qb_ctx_p, &camera);
}

/* tmporary stuff */

