#ifndef __QBDEMO_SHAPES_H_
#define __QBDEMO_SHAPES_H_

#include <qb.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Macros */

/* Typedefs */

/* Interface */
void qbd_shp_box_insert(qb_context_t *qb_ctx_p,
                        qb_icoord_t size, qb_icoord_t off,
                        qb_color_t color);
void qbd_shp_tiled_box_insert(qb_context_t *qb_ctx_p,
                              qb_icoord_t size, qb_icoord_t offset,
                              qb_icoord_t tile_offset, int tile_size,
                              qb_color_t color1, qb_color_t color2);

#ifdef __cplusplus
}
#endif

#endif // __QBDEMO_SHAPES_H_
