#ifndef __QBDEMO_CAMERA_H_
#define __QBDEMO_CAMERA_H_

#include <qb.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Macros */
#define QBD_CAM_DIRECTION_LEFT     1
#define QBD_CAM_DIRECTION_RIGHT    2

/* Typedefs */
typedef struct qbd_cam_slider {
  float y;
  float xz_dist;
  float angle;
  float wanted_angle;
  float time_left;
} qbd_cam_slider_t;


/* Interface */
int qbd_cam_slider_create(qbd_cam_slider_t **slider_pp, qb_camera_t *cam_p);
void qbd_cam_slider_destroy(qbd_cam_slider_t **slider_pp);
void qbd_cam_slider_to_side(qbd_cam_slider_t *slider_p, int side, float speed);
void qbd_cam_slider_flip_side(qbd_cam_slider_t *slider_p, int dir, float speed);
void qbd_cam_slider_add_angle(qbd_cam_slider_t *slider_p,
                              float angle, float speed);
int qbd_cam_slider_advance(qbd_cam_slider_t *slider_p, float time);
void qbd_cam_slider_apply(qb_context_t *qb_ctx_p, qbd_cam_slider_t *slider_p);

void qbd_cam_set_gap(qb_context_t *qb_ctx_p, int use_gap);

#ifdef __cplusplus
}
#endif

#endif // __QBDEMO_CAMERA_H_
