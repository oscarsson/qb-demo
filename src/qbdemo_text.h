#ifndef __QBDEMO_TEXT_H_
#define __QBDEMO_TEXT_H_

#include <qb.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Macros */

/* Typedefs */
typedef struct qbd_txt_character {
  int w;
  unsigned char *pixels;
} qbd_txt_character_t;

typedef struct qbd_txt_font {
  int h;
  int base;
  qbd_txt_character_t chars[256];
} qbd_txt_font_t;

typedef struct qbd_txt {
  const char *string; /* utf8 */
  qbd_txt_font_t *font_p;
} qbd_txt_t;

typedef struct qbd_txt_scroll {
  qbd_txt_font_t *font_p;
  char *str;
  unsigned char *pixels;
  int w;
  int h;
  int len;
  float speed; /* pixels/s */
  float offset;
} qbd_txt_scroll_t;

/* Interface */
int qbd_txt_font_load(qbd_txt_font_t **font_pp, const char *path,
                          int size);
void qbd_txt_font_unload(qbd_txt_font_t **font_pp);
int qbd_txt_get_width(qbd_txt_t *text_p);
void qbd_txt_get_pixels(unsigned char *out_pix, int w,
                            qbd_txt_t *text_p,
                            int off_x);

int qbd_txt_scroll_create(qbd_txt_scroll_t **scroll_pp,
                              qbd_txt_t *text_p,
                              int w, float speed);
void qbd_txt_scroll_destroy(qbd_txt_scroll_t **scroll_pp);
int qbd_txt_scroll_advance(qbd_txt_scroll_t *scroll_p, float time);

void qbd_txt_string_insert_basic(qb_context_t *qb_ctx_p,
                                     qbd_txt_t *text_p,
                                     qb_icoord_t off, qb_color_t color);
void qbd_txt_scroll_insert_basic(qb_context_t *qb_ctx_p,
                                     qbd_txt_scroll_t *scroll_p,
                                     qb_icoord_t off, qb_color_t color);

#ifdef __cplusplus
}
#endif

#endif // __QBDEMO_TEXT_H_
