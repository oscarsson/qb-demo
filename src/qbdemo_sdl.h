#ifndef __QBDEMO_SDL_H_
#define __QBDEMO_SDL_H_

#include <stdint.h>
#include <pthread.h>
#include <sys/queue.h>
#include <SDL2/SDL.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Macros */
#define QBD_SDL_KEYEVENT_DOWN            1
#define QBD_SDL_KEYEVENT_UP              2
#define QBD_SDL_KEYEVENT_DOWN_REPEAT     4

#define QBD_SDL_MOUSE_BL                 0
#define QBD_SDL_MOUSE_BM                 1
#define QBD_SDL_MOUSE_BR                 2
#define QBD_SDL_MOUSE_BL_BIT             (1 << QBD_SDL_MOUSE_BL)
#define QBD_SDL_MOUSE_BM_BIT             (1 << QBD_SDL_MOUSE_BM)
#define QBD_SDL_MOUSE_BR_BIT             (1 << QBD_SDL_MOUSE_BR)
#define QBD_SDL_MOUSE_NUM_BUTTONS        3


/* Typedefs */
/* define the 'struct event_list_head' */
TAILQ_HEAD(event_list_head, event);
typedef struct event_list_head event_list_head_t;

typedef struct qbd_sdl_mouse_state {
  int click_start_time[QBD_SDL_MOUSE_NUM_BUTTONS];
  int clicks[QBD_SDL_MOUSE_NUM_BUTTONS];
  int buttons_pressed;
  int buttons_drag;
  int relative_mode;
  int x;
  int y;
  int x_rel;
  int y_rel;
  int x_saved;
  int y_saved;
  int x_wheel;
  int y_wheel;
} qbd_sdl_mouse_state_t;

typedef struct qbd_sdl_window {
  TAILQ_ENTRY(qbd_sdl_window) window_list;
  event_list_head_t event_head; /* events for this window */
  pthread_mutex_t event_mutex;
  int skip_events;

  SDL_Window *window;
  uint32_t window_id;

  uint8_t key_pressed[SDL_NUM_SCANCODES];
  uint8_t key_events[SDL_NUM_SCANCODES];
  uint8_t window_active;
  uint8_t quit;
  qbd_sdl_mouse_state_t mouse;
} qbd_sdl_window_t;


/* Interface */
int qbd_sdl_init(int *got_quit_event_p);
void qbd_sdl_exit(void);
void qbd_sdl_pump_events(void);
void qbd_sdl_handle_events(qbd_sdl_window_t *win_p);
void qbd_sdl_window_open(qbd_sdl_window_t **win_pp, const char* title,
                         int w, int h, uint32_t flags,
                         int skip_events);
void qbd_sdl_window_close(qbd_sdl_window_t **win_pp);
void qbd_sdl_window_set_fullscreen(qbd_sdl_window_t *win_p, int fullscreen);
void qbd_sdl_key_clear_events(qbd_sdl_window_t *win_p);
void qbd_sdl_mouse_set_relative_mode(qbd_sdl_window_t *win_p, int be_relative);
void qbd_sdl_mouse_clear_relative_xy(qbd_sdl_window_t *win_p);

#ifdef __cplusplus
}
#endif

#endif // __QBDEMO_SDL_H_
