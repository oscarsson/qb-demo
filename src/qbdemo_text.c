#include <unistd.h>
#include <SDL2/SDL_ttf.h>

#include "qbdemo_text.h"

/* Macros */

/* Typedefs */

/* Local variables */
static int ttf_is_init;

/* Prototypes */

/* Local functions */
static inline int utf8_char_len(unsigned char c)
{
    if (c < 0x80 ) return 1;
    if ((c & 0x20) == 0) return 2;
    if ((c & 0x10) == 0) return 3;
    if ((c & 0x08) == 0) return 4;
    if ((c & 0x04) == 0) return 5;
    return 6;
}

static unsigned char *utf8_char_to_latin1(unsigned char *out_p,
                                          unsigned char *in_p)
{
  unsigned int v;
  int len = utf8_char_len(*in_p);

  if (len == 1) {
    *out_p = *in_p;
    in_p++;

    return in_p;
  }

  v = (*in_p & (0xff >> (len + 1))) << ((len - 1) * 6);
  in_p++;
  for (len--; len > 0; len--) {
    v |= (*in_p - 0x80) << ((len - 1) * 6);
    in_p++;
  }

  *out_p = (v > 0xff) ? (unsigned char)'_' : (unsigned char)v;
  return in_p;
}

static void utf8_str_to_latin1(char *out_str, char *in_str)
{
  unsigned char *out_c = (unsigned char *)out_str;
  unsigned char *in_c = (unsigned char *)in_str;

  while (1) {
    if ( *in_c == 0 ) {
      *out_c = 0;
      break;
    }

    in_c = utf8_char_to_latin1(out_c, in_c);
    out_c++;
  }
}

/* str is latin1 string */
static void get_string_pixels(unsigned char *out_pix, int w,
                              qbd_txt_font_t *font_p, const char *str,
                              int off_x)
{
  int i, x, y, done_x;
  unsigned char c;
  unsigned char *font_pix;
  int font_w, h;

  if (w < 0) {
    printf("Failed to get text pixels, negative width\n");
    return;
  }

  h = font_p->h;
  memset(out_pix, 0, w * h);

  if (w + off_x <= 0) {
    return;
  }

  i = 0;
  if (off_x < 0) {
    done_x = -off_x;
    off_x = 0;
  } else {
    done_x = 0;
    /* fast forward to first visible character */
    for (i = 0; ; i++) {
      if (!str[i]) {
        return;
      }
      c = (unsigned char)str[i];
      if (off_x - font_p->chars[c].w <= 0) {
        break;
      }
      off_x -= font_p->chars[c].w;
    }
  }

  /* now off_x is >= 0 and holds how many pixels should be
     removed from current character */
  for ( ; (done_x < w - 1) && str[i]; i++) {
    c = (unsigned char)str[i];
    font_pix = font_p->chars[c].pixels;
    font_w = font_p->chars[c].w;
    for (y = 0; y < h; y++) {
      for (x = off_x; (x < font_w) && (done_x + x < w); x++) {
        out_pix[y * w + (done_x + x - off_x)] = font_pix[y * font_w + x];
      }
    }
    done_x += (font_w - off_x);
    off_x = 0;
  }
}

/* Interface */

int qbd_txt_font_load(qbd_txt_font_t **font_pp, const char *path, int size)
{
  qbd_txt_font_t *font_p;
  TTF_Font *ttf_font_p;
  SDL_Surface *surface_p;
  SDL_Color sdl_white = {255, 255, 255};
  unsigned char *pix; /* only one byte per pixel */
  int i, w, h, x, y, yf;
  char str[2] = {0};

  if (!ttf_is_init) {
    if (TTF_Init() == -1) {
      printf( "Error initializing SDL_ttf: %s\n", TTF_GetError());
      return 1;
    }
    ttf_is_init = 1;
  }

  ttf_font_p = TTF_OpenFont(path, size);
  if (ttf_font_p == NULL) {
    printf( "Failed to load font: %s\n", TTF_GetError());
    return 1;
  }

  /* use L to find base and 'normal' hight */
  surface_p = TTF_RenderText_Solid(ttf_font_p, "L", sdl_white);
  if (!surface_p || surface_p->h < 1) {
    printf( "Failed to load font: No L\n");
    return 1;
  }

  font_p = malloc(sizeof(qbd_txt_font_t));
  font_p->h = surface_p->h;
  font_p->base = -1;

  pix = (unsigned char *)surface_p->pixels;
  /* surface got pixels with y axis going down, start at bottom */
  for (y = surface_p->h - 1; y >= 0 && font_p->base < 0; y--) {
    for (x = 0; x < surface_p->w; x++) {
      if (pix[(y * surface_p->pitch) + x]) {
        font_p->base = surface_p->h - y - 1;
        break;
      }
    }
  }
  if (font_p->base < 0) {
    printf( "Failed to load font: No base\n");
    free(font_p);
    return 1;
  }

  for (i = 0; i < 256; i++) {
    str[0] = (char)i;
    surface_p = TTF_RenderText_Solid(ttf_font_p, str, sdl_white);
    if (!surface_p) {
      font_p->chars[i].w = 0;
      font_p->chars[i].pixels = NULL;
      continue;
    }

    w = surface_p->w;
    font_p->chars[i].w = surface_p->w;
    /* strip top if to high, leave blank (calloc) if to low */
    h = surface_p->h < font_p->h ? surface_p->h : font_p->h;

    font_p->chars[i].pixels = calloc(w * font_p->h, sizeof(char));
    pix = (unsigned char *)surface_p->pixels;
    /* swap y since we have positive y up and surface have it down */
    for (y = surface_p->h - 1, yf = 0;
         yf < h; yf++, y--) {
      memcpy(&font_p->chars[i].pixels[yf * w],
             &pix[y * surface_p->pitch], w);
    }

    SDL_FreeSurface(surface_p);
  }

  *font_pp = font_p;
  return 0;
}

void qbd_txt_font_unload(qbd_txt_font_t **font_pp)
{
  qbd_txt_font_t *font_p = *font_pp;
  int i;

  for (i = 0; i < 256; i++) {
    if (font_p->chars[i].pixels) {
      free(font_p->chars[i].pixels);
    }
  }
  free(font_p);

  *font_pp = NULL;
}

int qbd_txt_get_width(qbd_txt_t *text_p)
{
  int w;
  char *str;
  unsigned char *c;

  str = strdup(text_p->string);
  utf8_str_to_latin1(str, str); /* in place */

  w = 0;
  for (c = (unsigned char *)str; *c; c++) {
    w += text_p->font_p->chars[*c].w;
  }

  free(str);
  return w;
}

/* height is taken from font height */
void qbd_txt_get_pixels(unsigned char *out_pix, int w,
                        qbd_txt_t *text_p, int off_x)
{
  char *str;

  str = strdup(text_p->string);
  utf8_str_to_latin1(str, str); /* in place */
  get_string_pixels(out_pix, w, text_p->font_p, str, off_x);
  free(str);
}

int qbd_txt_scroll_create(qbd_txt_scroll_t **scroll_pp,
                          qbd_txt_t *text_p, int w, float speed)
{
  qbd_txt_scroll_t *scroll_p = malloc(sizeof(qbd_txt_scroll_t));

  scroll_p->font_p = text_p->font_p;
  scroll_p->str = strdup(text_p->string);
  utf8_str_to_latin1(scroll_p->str, scroll_p->str); /* in place */
  scroll_p->pixels = calloc(w * text_p->font_p->h, sizeof(unsigned char));
  scroll_p->w = w;
  scroll_p->h = text_p->font_p->h;
  scroll_p->len = qbd_txt_get_width(text_p);
  scroll_p->speed = speed;
  scroll_p->offset = -1.0f * (float)w;

  get_string_pixels(scroll_p->pixels, w, scroll_p->font_p,
                    scroll_p->str, (int)floor(scroll_p->offset));

  *scroll_pp = scroll_p;
  return 0;
}

void qbd_txt_scroll_destroy(qbd_txt_scroll_t **scroll_pp)
{
  qbd_txt_scroll_t *scroll_p = *scroll_pp;

  if (scroll_p->str) {
    free(scroll_p->str);
  }
  if (scroll_p->pixels) {
    free(scroll_p->pixels);
  }

  free(scroll_p);
  *scroll_pp = NULL;
}

int qbd_txt_scroll_advance(qbd_txt_scroll_t *scroll_p, float time)
{
  int off;

  scroll_p->offset += (time * scroll_p->speed);
  off = (int)floor(scroll_p->offset);

  get_string_pixels(scroll_p->pixels, scroll_p->w, scroll_p->font_p,
                    scroll_p->str, off);

  return (off > scroll_p->len + scroll_p->w);
}


void qbd_txt_string_insert_basic(qb_context_t *qb_ctx_p,
                                 qbd_txt_t *text_p,
                                 qb_icoord_t off, qb_color_t color)
{
  int x, y, w, h;
  unsigned char *pixels;
  qb_voxel_t vxl;

  w = qbd_txt_get_width(text_p);
  h = text_p->font_p->h;
  pixels = malloc(w * h);
  qbd_txt_get_pixels(pixels, w, text_p, 0);

  vxl.pos.z = 0;
  vxl.color = color;
  for (y = h - 1; y >= 0; y--) {
    for (x = 0; x < w; x++) {
      if (pixels[y * w + x]) {
        vxl.pos.x = x;
        vxl.pos.y = y;
        qb_voxel_set(qb_ctx_p, &vxl, &off);
      }
    }
  }

  free(pixels);
}

void qbd_txt_scroll_insert_basic(qb_context_t *qb_ctx_p,
                                 qbd_txt_scroll_t *scroll_p,
                                 qb_icoord_t off, qb_color_t color)
{
  int x, y, w, h;
  qb_voxel_t vxl;

  w = scroll_p->w;
  h = scroll_p->h;
  vxl.pos.z = 0;
  vxl.color = color;
  for (y = h - 1; y >= 0; y--) {
    for (x = 0; x < w; x++) {
      if (scroll_p->pixels[y * w + x]) {
        vxl.pos.x = x;
        vxl.pos.y = y;
        qb_voxel_set(qb_ctx_p, &vxl, &off);
      }
    }
  }
}

/* tmporary stuff */




